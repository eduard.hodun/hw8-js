Теоретичні питання

1.Опишіть своїми словами що таке Document Object Model (DOM)

    Це глобальний обєкт, який на основі HTML коду будує деревоподібну структуру вузлів html тегів, роблячи їх обєктами 
    до яких легко можна звертатится черес js.


2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?

    innerHTML - відображає html розмітку + текст, а
    innerText - відображає вміст HTML-елемента

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

    за допомогою:
                -ID
                -ClassName
                -Тегу
                -Атрибуту

// // замінимо колір фону на червоний,
// document.body.style.background = "red";
//
// // а через секунду повернемо як було
// setTimeout(() => document.body.style.background = "", 3000);

Завдання
Код для завдань лежить в папці project.


1.Знайти всі параграфи на сторінці та встановити колір фону #ff0000


2.Знайти елемент із id="optionsList". Вивести у консоль. 
Знайти батьківський елемент та вивести в консоль. 
Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.


3.Встановіть в якості контента елемента з id="testParagraph" наступний параграф -
This is a paragraph



4.Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
Кожному з елементів присвоїти новий клас nav-item.


5.Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


