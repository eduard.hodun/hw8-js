// TASK №1
const bgArticles = document.getElementsByTagName('p');

    for (let article of bgArticles) {

        article.style.backgroundColor = '#ff0000'

    }

// TASK №2
const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const perentElement = optionsList.parentElement;
console.log(perentElement);

const childrenOptionsList = optionsList.childNodes;

for (let node of childrenOptionsList) {

    console.log(`${node.nodeType}`);
    console.log(`${node.nodeName}`);

}



// TASK №3
const testParagraph = document.getElementById('testParagraph');

testParagraph.textContent = "This is a paragraph";
console.log(testParagraph);




// TASK №4

const mainHeaderElements = document.querySelector('.main-header');

for (let element of mainHeaderElements.children) {

    element.classList.add("nav-item");
}
console.log(mainHeaderElements);


// TASK №5

const sectionTitles = document.querySelectorAll('.section-title');
console.log(sectionTitles);

for (let title of sectionTitles) {

    title.classList.remove('section-title');
}

console.log(sectionTitles);

